import React from 'react'
import Axios from 'axios'
import {withStyles} from "@material-ui/core";

class AxiosConnector extends React.Component {

    constructor(props){
        super(props);
        this.axios = Axios

    }

    state = {
        LOCAL: {
            "SOS": "http://localhost:8080",
            "WEB_SERVICE": "http://localhost:5000",
        },

    };

    listObjects = (bucket_name) => {
        const {SOS, WEB_SERVICE} = this.state.LOCAL;
        return this.axios.get(`${WEB_SERVICE}/api/${bucket_name}/show`)
            .then(res => {

                const a = res.data.map(itm => {
                    itm["src"] = `${SOS}/${bucket_name}/${itm.name}`;
                    return itm
                })

                console.log(a);
                return a
            })
    };


    executeJob = (bucket_name, object_name) => (e) => {
        const {SOS, WEB_SERVICE} = this.state.LOCAL;
        this.axios.post(`${WEB_SERVICE}/api/workqueue/job`, {
            bucket_name, object_name
        })
    };

    executeBatchJob = (bucket_name) => (e) => {
        const {SOS, WEB_SERVICE} = this.state.LOCAL;
        this.axios.post(`${WEB_SERVICE}/api/workqueue/job/all`, {bucket_name})
    };

    removeOne = (bucket_name, object_name) => (e) => {
        console.log(bucket_name, object_name)
        const {SOS, WEB_SERVICE} = this.state.LOCAL;
        this.axios.post(`${WEB_SERVICE}/api/remove`, {
            bucket_name, object_name
        })
    };

    removeAll = (bucket_name) => (e) => {
        const {SOS, WEB_SERVICE} = this.state.LOCAL;
        this.axios.post(`${WEB_SERVICE}/api/remove/all`, {
            bucket_name
        })
    };


    render() {
        const func = {
            listObjects: this.listObjects,
            executeJob: this.executeJob,
            executeBatchJob: this.executeBatchJob,
            removeOne: this.removeOne,
            removeAll: this.removeAll
        };

        const {
            children
        } = this.props

        return React.Children.map(children, (child) => {
            return React.cloneElement(child, {
                ...func
            });
        });

    }
}

export default AxiosConnector
