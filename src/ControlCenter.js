import React from 'react'
import Typography from "@material-ui/core/Typography";
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import _ from 'lodash'
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';

const styles = theme => ({
    card: {
        display: 'flex',
    }
});

const VideoItem = ({classes, children: text, onMakeThumbnailClicked}) => {
    return (
        <Card className={classes.card}>
            <CardContent>
                <Typography gutterBottom variant="headline" component="h2">
                    {text}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small" color="primary" onClick={onMakeThumbnailClicked}>
                    Make Thumbnail
                </Button>
            </CardActions>
        </Card>
    )
}


class ControlCenter extends React.Component {

    state = {
        bucket_name: "",
        objects: [
        ]
    }

    componentDidMount() {
        console.log("Refresh")
        this.props.listObjects(this.props.match.params.bucketName)
            .then(data => {
                this.setState(() => {
                    return {
                        objects: _.filter(data, ((d) => !_.endsWith(d.name, ".gif")))
                    }
                }, () => console.log(this.state.objects))
            })
    }

    render() {

        const {
            classes,
            match : {
                params: {
                    bucketName
                }

            }
        } = this.props;

        const {
            objects
        } = this.state;

        return (
            <Card>
                <div>
                    <CardActions>
                        <Typography gutterBottom variant="headline" component="h2">
                            /{bucketName}
                        </Typography>
                        {
                            objects.length > 0 && (<Button size="small" color="primary" onClick={this.props.executeBatchJob(bucketName)}>
                                Make All Thumbnail
                            </Button>)
                        }
                    </CardActions>
                </div>
                {
                    objects.length > 0 && objects.map((itm, i) => (
                        <VideoItem key={i} classes={classes} onMakeThumbnailClicked={this.props.executeJob(bucketName, itm.name)}>
                            {itm.name}
                        </VideoItem>
                    ))
                }
                {
                    objects.length === 0 && <div align="center"><div>No Item</div></div>
                }
            </Card>
        );
    }
}

export default withStyles(styles)(ControlCenter)
