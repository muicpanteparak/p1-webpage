import React from 'react'
import Grid from "@material-ui/core/Grid";
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import {withStyles} from "@material-ui/core";
import GridList from "@material-ui/core/GridList";
import GridListTile from "@material-ui/core/GridListTile";
import _ from "lodash";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions/CardActions";
import Typography from "@material-ui/core/Typography/Typography";
import Button from "@material-ui/core/Button/Button";

const styles = theme => ({
    titleBar: {
        background:
            'linear-gradient(to bottom, rgba(0,0,0,0.7) 0%, ' +
            'rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
    },
    icon: {
        color: 'white',
    },
    gridList: {
        width: 500,
        height: 450,
        transform: 'translateZ(0)',
    },
})

const GIFItems = ({name, onDelete, src, classes}) => {
    console.log(src)
    return (
        <GridListTile cols={1} rows={1}>
            <img src={src} />
            <GridListTileBar
                title={name}
                titlePosition="top"
                actionIcon={
                    <IconButton className={classes.icon} onClick={onDelete}>
                        <DeleteIcon />
                    </IconButton>
                }
                actionPosition="left"
                className={classes.titleBar}
            />
        </GridListTile>
    )
}

class DisplayCenter extends React.Component {

    state = {
        bucket_name: "",
        objects: [],
    };

    componentDidMount() {

        this.props.listObjects(this.props.match.params.bucketName)
            .then(data => {
                this.setState({
                    objects: _.filter(data, ((d) => _.endsWith(d.name, ".gif")))
                })
            })
    }

    render() {
        const {
            classes
        } = this.props;

        const {
            objects,
        } = this.state;

        console.log("props", this.props)
        return (
            <Card>
                <div>
                    <CardActions>
                        <Typography gutterBottom variant="headline" component="h2">
                            /{this.props.match.params.bucketName}
                        </Typography>
                        {
                            objects.length > 0 && (<Button size="small" color="primary" onClick={this.props.removeAll(this.props.match.params.bucketName)}>
                                Delete All Thumbnail
                            </Button>)
                        }
                    </CardActions>
                </div>
                <Grid
                    container
                    direction="row"
                    justify="space-evenly"
                    alignItems="flex-start"
                >

                    {
                        objects.length > 0 && objects.map(itm => (
                            <GIFItems name={itm.name} src={itm.src} classes={classes} onDelete={this.props.removeOne(this.props.match.params.bucketName, itm.name)}/>
                        ))
                    }

                    {
                        objects.length === 0 && <Grid item xs={12}><div>No Item</div></Grid>
                    }
                </Grid>
            </Card>
        )
    }
}

export default withStyles(styles) (DisplayCenter)
