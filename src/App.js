import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import { withStyles } from '@material-ui/core/styles';
import DisplayCenter from "./DisplayCenter";
import AxiosConnector from "./AxiosConnector";
import ControlCenter from "./ControlCenter";

const styles = {
    root: {
        flexGrow: 1,
    },
};

const AxiosRoute = ({component: Component, path, exact, ...rest}) => {
    return (
        <Route path={path} exact={exact} render={props => (<AxiosConnector><Component {...props} {...rest} /></AxiosConnector>)}  />
    )
}


class App extends Component {

    render() {
        return (
            <div>
                <AxiosRoute path="/:bucketName/control" component={ControlCenter} />
                <AxiosRoute path="/:bucketName/display" component={DisplayCenter} />
            </div>
        );
    }
}

export default (App);
